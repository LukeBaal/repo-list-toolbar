const user = 'LukeBaal';

const createColletionItem = project => {
  const a = document.createElement('a');
  const i = document.createElement('i');

  a.className = 'collection-item';
  i.className = project.logo;

  a.appendChild(i);
  a.innerHTML += ` ${project.name[0].toUpperCase()}${project.name.slice(1)}`;
  a.href = project.link;
  a.target = '_blank';

  return a;
};

const getRepos = (url, logo, link, updated) =>
  fetch(url)
    .then(response => response.json())
    .then(projects =>
      projects.map(project => {
        return {
          name: project.name,
          logo: logo,
          link: project[link],
          updated: project[updated]
        };
      })
    );

let sources = [];

// Github
sources.push(
  getRepos(
    `https://api.github.com/users/${user}/repos`,
    'fab fa-github',
    'html_url',
    'updated_at'
  )
);

// Gitlab
sources.push(
  getRepos(
    `https://gitlab.com/api/v4/users/${user}/projects`,
    'fab fa-gitlab',
    'web_url',
    'last_activity_at'
  )
);

Promise.all(sources)
  .then(values => [].concat(...values))
  .then(projects => {
    // Sort by updated date
    projects = projects.sort((a, b) => {
      a = new Date(a.updated);
      b = new Date(b.updated);
      return a > b ? -1 : a < b ? 1 : 0;
    });

    // Query for list
    const collection = document.querySelector('.collection');

    // Create an entry for each project and add to list
    projects.forEach(project => {
      const element = createColletionItem(project);
      collection.appendChild(element);
    });
  });
